package com.aitekteam.developer.d_laundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aitekteam.developer.d_laundry.R;
import com.aitekteam.developer.d_laundry.interfaces.PackageUnitInterface;
import com.aitekteam.developer.d_laundry.models.M_Package_Unit;

import java.util.ArrayList;

public class OrderPackageUnitAdapters extends BaseAdapter {
    private ArrayList<M_Package_Unit> items;
    private Context context;
    private LayoutInflater inflater;
    private PackageUnitInterface handler;

    public OrderPackageUnitAdapters(Context context, PackageUnitInterface handler) {
        this.items = new ArrayList<>();
        addTotal();
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.handler = handler;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int i) {
        return this.items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private void addTotal() {
        int total = 0;
        for (M_Package_Unit item: this.items) {
            total += item.getItem_price();
        }
        this.items.add(new M_Package_Unit(
                -1,
                total,
                "Total"
        ));
    }

    public void addItem(M_Package_Unit item) {
        this.items.remove(this.items.size() - 1);
        this.items.add(item);
        addTotal();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        if (position != this.items.size() - 1) {
            this.items.remove(this.items.size() - 1);
            this.items.remove(position);
            addTotal();
            notifyDataSetChanged();
        }
    }

    public ArrayList<M_Package_Unit> getItems() {
        return this.items;
    }

    private class Holder {
        TextView
                package_unit_name,
                package_unit_price;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View root = this.inflater.inflate(R.layout.item_order, null);
        holder.package_unit_name = root.findViewById(R.id.order_name);
        holder.package_unit_price = root.findViewById(R.id.order_price);

        holder.package_unit_name.setText(this.items.get(i).getItem_name());
        holder.package_unit_price.setText("Rp. " + this.items.get(i).getItem_price() + ".000");

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.onClick(items.get(i), i);
            }
        });

        return root;
    }
}
