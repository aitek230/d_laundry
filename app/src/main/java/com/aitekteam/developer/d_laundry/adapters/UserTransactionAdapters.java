package com.aitekteam.developer.d_laundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aitekteam.developer.d_laundry.R;
import com.aitekteam.developer.d_laundry.interfaces.UserTransactionInterface;
import com.aitekteam.developer.d_laundry.models.M_Users_Transaction;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class UserTransactionAdapters extends BaseAdapter {
    private ArrayList<M_Users_Transaction> items;
    private Context context;
    private LayoutInflater inflater;
    private UserTransactionInterface handler;

    public UserTransactionAdapters(ArrayList<M_Users_Transaction> items, Context context, UserTransactionInterface handler) {
        this.items = items;
        this.context = context;
        this.handler = handler;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int i) {
        return this.items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class Holder {
        TextView
                user_payment_name,
                user_payment_price;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {Holder holder = new Holder();
        View root = this.inflater.inflate(R.layout.item_order, null);
        holder.user_payment_name = root.findViewById(R.id.order_name);
        holder.user_payment_price = root.findViewById(R.id.order_price);

        holder.user_payment_name.setText(this.items.get(i).getTransaction_description());
        holder.user_payment_price.setText(convertTime(this.items.get(i).getTransaction_date()));

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.onClick(items.get(i), i);
            }
        });

        return root;
    }

    private String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss", Locale.ENGLISH);
        return format.format(date);
    }
}
