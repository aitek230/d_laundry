package com.aitekteam.developer.d_laundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aitekteam.developer.d_laundry.R;
import com.aitekteam.developer.d_laundry.interfaces.OutletInterface;
import com.aitekteam.developer.d_laundry.models.M_Provider;

import java.util.ArrayList;

public class OutletAdapters extends BaseAdapter {

    private ArrayList<M_Provider> items;
    private Context context;
    private LayoutInflater inflater;
    private OutletInterface handler;

    public OutletAdapters(ArrayList<M_Provider> items, Context context, OutletInterface handler) {
        this.items = items;
        this.context = context;
        this.handler = handler;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int i) {
        return this.items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public ArrayList<M_Provider> getItems() {
        return this.items;
    }

    public void setItems(ArrayList<M_Provider> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    private class Holder {
        TextView
                provider_logo,
                provider_name,
                provider_address;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View root = this.inflater.inflate(R.layout.item_outlet, null);
        holder.provider_logo = root.findViewById(R.id.provider_logo);
        holder.provider_name = root.findViewById(R.id.provider_name);
        holder.provider_address = root.findViewById(R.id.provider_address);

        holder.provider_logo.setText(this.items.get(i).getProvider_name().substring(0, 2));
        holder.provider_name.setText(this.items.get(i).getProvider_name());
        holder.provider_address.setText(this.items.get(i).getProvider_address());

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.onClick(items.get(i), i);
            }
        });

        return root;
    }
}
