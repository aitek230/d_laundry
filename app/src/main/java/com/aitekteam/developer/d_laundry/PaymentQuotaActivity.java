package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.aitekteam.developer.d_laundry.adapters.PaymentAdapters;
import com.aitekteam.developer.d_laundry.interfaces.PaymentInterface;
import com.aitekteam.developer.d_laundry.models.M_Package_Member;
import com.aitekteam.developer.d_laundry.models.M_Payment;
import com.aitekteam.developer.d_laundry.models.M_Users;
import com.aitekteam.developer.d_laundry.models.M_Users_Transaction;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class PaymentQuotaActivity extends AppCompatActivity {

    private TextView payment_total;
    private GridView payment_list;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser currentUser;
    private M_Package_Member quota;
    private M_Users users;
    private Integer source;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_quota);

        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = this.mAuth.getCurrentUser();
        this.mDatabase = FirebaseDatabase.getInstance().getReference();

        this.payment_total = findViewById(R.id.payment_total);
        this.payment_list = findViewById(R.id.payment_list);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        if (args != null) {
            this.quota = (M_Package_Member) args.getSerializable("QUOTA");
            this.users = (M_Users) args.getSerializable("USERS");
            this.source = (Integer) args.getSerializable("SOURCE");
            payment_total.setText("Total : Rp." + quota.getPackage_price() + ".000");

            getPayment();
        }
    }

    private void getPayment() {
        this.mDatabase.child("m-payment").addValueEventListener(payment);
    }

    private ValueEventListener payment = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 0) {
                ArrayList<M_Payment> items = new ArrayList<>();
                for (DataSnapshot item: dataSnapshot.getChildren()) {
                    items.add(item.getValue(M_Payment.class));
                }

                payment_list.setAdapter(new PaymentAdapters(items, PaymentQuotaActivity.this,
                        new PaymentInterface() {
                            @Override
                            public void onClick(M_Payment item, int position) {
                                setPayment(item);
                            }
                        }));
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void setPayment(M_Payment item) {
        if (quota != null && mAuth != null) {
            String transactionKey = mDatabase.child("m-users-transaction").child(mAuth.getUid()).push().getKey();
            M_Users_Transaction newUserTransaction = new M_Users_Transaction(
                    transactionKey,
                    quota.getPackage_name() + " Rp. " + quota.getPackage_price() + ".000",
                    1,
                    System.currentTimeMillis()
            );
            mDatabase.child("m-users-transaction").child(mAuth.getUid()).child(transactionKey).setValue(newUserTransaction);
            mDatabase.child("m-users").child(mAuth.getUid()).child("user_quota").setValue(
                    (users.getUser_quota() + quota.getPackage_kuota())
            ).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Berhasil menambah kuota", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Gagal menambah kuota", Toast.LENGTH_LONG).show();
                    }

                    if (source == 1) {
                        finish();
                    }
                    else {
                        Intent intent = new Intent(PaymentQuotaActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            });
        }
    }
}
