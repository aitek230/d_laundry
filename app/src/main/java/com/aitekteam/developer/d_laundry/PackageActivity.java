package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aitekteam.developer.d_laundry.adapters.UserTransactionAdapters;
import com.aitekteam.developer.d_laundry.interfaces.UserTransactionInterface;
import com.aitekteam.developer.d_laundry.models.M_Package_Member;
import com.aitekteam.developer.d_laundry.models.M_Users;
import com.aitekteam.developer.d_laundry.models.M_Users_Transaction;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class PackageActivity extends AppCompatActivity {

    private ImageView add_quota_member;
    private TextView quota_member;
    private ListView transaction_list;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser currentUser;
    private M_Users users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package);

        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = this.mAuth.getCurrentUser();
        this.mDatabase = FirebaseDatabase.getInstance().getReference();

        this.add_quota_member = findViewById(R.id.add_quota_member);
        this.quota_member = findViewById(R.id.quota_member);
        this.transaction_list = findViewById(R.id.transaction_list);

        getPackageList();
        getProfile();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (quota_member != null && mDatabase != null) {
            getProfile();
        }
    }

    private void getProfile() {
        if (currentUser != null)
            mDatabase.child("m-users").child(this.currentUser.getUid())
                    .addListenerForSingleValueEvent(profile);
    }

    private void getPackageList() {
        mDatabase.child("m-users-transaction").child(mAuth.getUid()).addValueEventListener(transaction);
    }

    private ValueEventListener transaction = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 0) {
                ArrayList<M_Users_Transaction> items = new ArrayList<>();
                for (DataSnapshot item: dataSnapshot.getChildren()) {
                    items.add(item.getValue(M_Users_Transaction.class));
                }
                if (items.size() > 0) {
                    setPackageList(items);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private ValueEventListener profile = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 1) {
                users = dataSnapshot.getValue(M_Users.class);
                if (users != null) {
                    quota_member.setText("Kuota : " + String.valueOf(users.getUser_quota()) + " Kg");
                    add_quota_member.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setAddQuotaMember(users);
                        }
                    });
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void setAddQuotaMember(M_Users item) {
        mDatabase.child("m-package-member").addValueEventListener(packageMember);
    }

    private ValueEventListener packageMember = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 0) {
                ArrayList<M_Package_Member> items = new ArrayList<>();
                for (DataSnapshot item : dataSnapshot.getChildren()) {
                    items.add(item.getValue(M_Package_Member.class));
                }
                if (items.size() > 0) {
                    setListPackageMember(items);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void setListPackageMember(final ArrayList<M_Package_Member> items) {
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_singlechoice);
        for (M_Package_Member item: items) {
            arrayAdapter.add(item.getPackage_name() +
                    " Rp. " +
                    item.getPackage_price() +
                    ".000");
        }
        if (arrayAdapter.getCount() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
            builder.setTitle(R.string.hint_choose_package)
                    .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(PackageActivity.this, PaymentQuotaActivity.class);
                            Bundle args = new Bundle();
                            args.putSerializable("QUOTA", (Serializable) items.get(i));
                            args.putSerializable("USERS", (Serializable) users);
                            args.putSerializable("SOURCE", (Serializable) 1);
                            intent.putExtra("BUNDLE", args);
                            startActivity(intent);
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void setPackageList(ArrayList<M_Users_Transaction> items) {
        this.transaction_list.setAdapter(new UserTransactionAdapters(items, this,
                new UserTransactionInterface() {
                    @Override
                    public void onClick(M_Users_Transaction item, int position) {

                    }
                }));
    }
}
