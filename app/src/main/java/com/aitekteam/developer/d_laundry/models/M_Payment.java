package com.aitekteam.developer.d_laundry.models;

import java.io.Serializable;

public class M_Payment implements Serializable {
    private int bank_id;
    private String
            bank_account_name,
            bank_name,
            bank_number;

    public M_Payment(int bank_id, String bank_account_name, String bank_name, String bank_number) {
        this.bank_id = bank_id;
        this.bank_account_name = bank_account_name;
        this.bank_name = bank_name;
        this.bank_number = bank_number;
    }

    public M_Payment() {
    }

    public int getBank_id() {
        return bank_id;
    }

    public void setBank_id(int bank_id) {
        this.bank_id = bank_id;
    }

    public String getBank_account_name() {
        return bank_account_name;
    }

    public void setBank_account_name(String bank_account_name) {
        this.bank_account_name = bank_account_name;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_number() {
        return bank_number;
    }

    public void setBank_number(String bank_number) {
        this.bank_number = bank_number;
    }
}
