package com.aitekteam.developer.d_laundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aitekteam.developer.d_laundry.R;
import com.aitekteam.developer.d_laundry.interfaces.PackageUnitInterface;
import com.aitekteam.developer.d_laundry.models.M_Package_Unit;

import java.util.ArrayList;

public class PackageUnitAdapters extends BaseAdapter {
    private ArrayList<M_Package_Unit> items;
    private Context context;
    private LayoutInflater inflater;
    private PackageUnitInterface handler;

    public PackageUnitAdapters(ArrayList<M_Package_Unit> items, Context context, PackageUnitInterface handler) {
        this.items = items;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.handler = handler;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int i) {
        return this.items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class Holder {
        TextView
                package_unit_logo,
                package_unit_name,
                package_unit_price;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View root = this.inflater.inflate(R.layout.item_outlet, null);
        holder.package_unit_logo = root.findViewById(R.id.provider_logo);
        holder.package_unit_name = root.findViewById(R.id.provider_name);
        holder.package_unit_price = root.findViewById(R.id.provider_address);

        holder.package_unit_logo.setText(this.items.get(i).getItem_name().substring(0, 2));
        holder.package_unit_name.setText(this.items.get(i).getItem_name());
        holder.package_unit_price.setText("Rp. " + this.items.get(i).getItem_price() + ".000");

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.onClick(items.get(i), i);
            }
        });

        return root;
    }
}
