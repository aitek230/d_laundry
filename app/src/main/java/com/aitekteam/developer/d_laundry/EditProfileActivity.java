package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aitekteam.developer.d_laundry.models.M_Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EditProfileActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser currentUser;
    private EditText full_name, password, phone;
    private Button button_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = this.mAuth.getCurrentUser();
        this.mDatabase = FirebaseDatabase.getInstance().getReference();

        this.full_name = findViewById(R.id.full_name);
        this.password = findViewById(R.id.password);
        this.phone = findViewById(R.id.phone);
        this.button_save = findViewById(R.id.button_save_profile);

        getProfile();
    }

    private void getProfile() {
        if (currentUser != null)
            mDatabase.child("m-users").child(this.currentUser.getUid())
                    .addListenerForSingleValueEvent(profile);
    }

    private ValueEventListener profile = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 1) {
                final M_Users users = dataSnapshot.getValue(M_Users.class);
                if (users != null) {
                    full_name.setText(users.getUser_full_name());
                    password.setText(users.getUser_password());
                    phone.setText(users.getUser_phone());
                    button_save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            users.setUser_full_name(full_name.getText().toString().trim());
                            users.setUser_password(password.getText().toString().trim());
                            users.setUser_phone(phone.getText().toString().trim());
                            button_save.setEnabled(false);
                            button_save.setBackgroundColor(getResources().getColor(R.color.colorPrimaryAlpha));
                            mDatabase.child("m-users").child(mAuth.getUid()).setValue(users)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(getApplicationContext(), "Berhasil menyimpan profile", Toast.LENGTH_LONG).show();
                                                finish();
                                            }
                                            else {
                                                Toast.makeText(getApplicationContext(), "Gagal menyimpan profile", Toast.LENGTH_LONG).show();
                                                button_save.setEnabled(true);
                                                button_save.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                            }
                                        }
                                    });
                        }
                    });
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
