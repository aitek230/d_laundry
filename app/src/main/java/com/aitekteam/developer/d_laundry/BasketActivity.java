package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aitekteam.developer.d_laundry.adapters.OrderPackageUnitAdapters;
import com.aitekteam.developer.d_laundry.adapters.PackageUnitAdapters;
import com.aitekteam.developer.d_laundry.interfaces.PackageUnitInterface;
import com.aitekteam.developer.d_laundry.models.M_Package_Unit;
import com.aitekteam.developer.d_laundry.models.M_Payment;
import com.aitekteam.developer.d_laundry.models.M_Provider;
import com.aitekteam.developer.d_laundry.models.M_Users;
import com.aitekteam.developer.d_laundry.models.M_Users_Payment_Package_Unit;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class BasketActivity extends AppCompatActivity {

    private GridView services_list;
    private ImageView user_face;
    private TextView
            user_name,
            user_email,
            user_phone;
    private OrderPackageUnitAdapters adapters;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser currentUser;
    private M_Provider selectedProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);

        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = this.mAuth.getCurrentUser();
        this.mDatabase = FirebaseDatabase.getInstance().getReference();

        this.user_face = findViewById(R.id.user_face);
        this.user_email = findViewById(R.id.user_email);
        this.user_name = findViewById(R.id.user_name);
        this.user_phone = findViewById(R.id.user_phone);
        this.services_list = findViewById(R.id.services_list);
        ListView order_list = findViewById(R.id.order_list);
        Button button_pay_later = findViewById(R.id.button_pay_later);
        Button button_pay_now = findViewById(R.id.button_pay_now);
        this.adapters = new OrderPackageUnitAdapters(this,
                new PackageUnitInterface() {
                    @Override
                    public void onClick(M_Package_Unit item, int position) {
                        adapters.removeItem(position);
                    }
                });
        order_list.setAdapter(this.adapters);

        int selected_provider = getIntent().getIntExtra("selected_provider", 0);

        getProfile();
        getServiceUnit(selected_provider);

        button_pay_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String key = mDatabase.child("m-user-payment-package-unit").child(mAuth.getUid()).push().getKey();
                M_Users_Payment_Package_Unit user_payment = new M_Users_Payment_Package_Unit(
                        adapters.getItems(),
                        selectedProvider,
                        new M_Payment(),
                        key,
                        System.currentTimeMillis()
                );
                mDatabase.child("m-user-payment-package-unit").child(mAuth.getUid())
                        .child(key).setValue(user_payment).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Berhasil membuat tagihan", Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Gagal membuat tagihan", Toast.LENGTH_LONG).show();
                        }
                        Intent intent = new Intent(BasketActivity.this, DetailOrderActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });

        button_pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapters.getItems().size() > 1 && selectedProvider != null) {
                    Intent intent = new Intent(BasketActivity.this, PaymentActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable("ARRAYLIST", (Serializable) adapters.getItems());
                    args.putSerializable("PROVIDER", (Serializable) selectedProvider);
                    intent.putExtra("BUNDLE", args);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void getServiceUnit(int selected_provider) {
        mDatabase.child("m-provider").child(String.valueOf(selected_provider))
                .addListenerForSingleValueEvent(provider);
    }

    private ValueEventListener provider = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 1) {
                selectedProvider = dataSnapshot.getValue(M_Provider.class);
                ArrayList<M_Package_Unit> items = new ArrayList<>();
                for (DataSnapshot item: dataSnapshot.child("package-unit").getChildren()) {
                    items.add(item.getValue(M_Package_Unit.class));
                }

                services_list.setAdapter(new PackageUnitAdapters(items, BasketActivity.this,
                    new PackageUnitInterface() {
                        @Override
                        public void onClick(M_Package_Unit item, int position) {
                            adapters.addItem(item);
                        }
                    }));
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void getProfile() {
        if (currentUser != null)
            mDatabase.child("m-users").child(this.currentUser.getUid())
                    .addListenerForSingleValueEvent(profile);
    }

    private ValueEventListener profile = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 1) {
                M_Users users = dataSnapshot.getValue(M_Users.class);
                if (users != null) {
                    user_email.setText(users.getUser_email());
                    user_name.setText(users.getUser_full_name());
                    user_phone.setText(users.getUser_phone());
                    user_face.setImageResource(R.drawable.user);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
