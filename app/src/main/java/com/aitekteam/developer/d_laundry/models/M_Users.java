package com.aitekteam.developer.d_laundry.models;

import java.io.Serializable;

public class M_Users implements Serializable {
    private String
            user_id,
            user_full_name,
            user_email,
            user_password,
            user_phone,
            user_package_name;
    private int
            user_type,
            package_member_id,
            package_status,
            user_quota;

    public M_Users(String user_id, String user_full_name, String user_email, String user_password, String user_phone, String user_package_name, int user_type, int package_member_id, int package_status, int user_quota) {
        this.user_id = user_id;
        this.user_full_name = user_full_name;
        this.user_email = user_email;
        this.user_password = user_password;
        this.user_phone = user_phone;
        this.user_package_name = user_package_name;
        this.user_type = user_type;
        this.package_member_id = package_member_id;
        this.package_status = package_status;
        this.user_quota = user_quota;
    }

    public M_Users() {
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_full_name() {
        return user_full_name;
    }

    public void setUser_full_name(String user_full_name) {
        this.user_full_name = user_full_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_package_name() {
        return user_package_name;
    }

    public void setUser_package_name(String user_package_name) {
        this.user_package_name = user_package_name;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public int getPackage_member_id() {
        return package_member_id;
    }

    public void setPackage_member_id(int package_member_id) {
        this.package_member_id = package_member_id;
    }

    public int getPackage_status() {
        return package_status;
    }

    public void setPackage_status(int package_status) {
        this.package_status = package_status;
    }

    public int getUser_quota() {
        return user_quota;
    }

    public void setUser_quota(int user_quota) {
        this.user_quota = user_quota;
    }
}
