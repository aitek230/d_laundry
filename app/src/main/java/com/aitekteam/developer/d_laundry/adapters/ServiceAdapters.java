package com.aitekteam.developer.d_laundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aitekteam.developer.d_laundry.R;

import java.util.ArrayList;

public class ServiceAdapters extends BaseAdapter {

    private ArrayList<String> items;
    private Context context;
    private LayoutInflater inflater;

    public ServiceAdapters(ArrayList<String> items, Context context) {
        this.items = items;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int i) {
        return this.items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class Holder {
        TextView
                service_logo,
                service_name;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View root = this.inflater.inflate(R.layout.item_service, null);
        holder.service_logo = root.findViewById(R.id.service_logo);
        holder.service_name = root.findViewById(R.id.service_name);

        holder.service_logo.setText(this.items.get(i).substring(0, 2));
        holder.service_name.setText(this.items.get(i));

        return root;
    }
}
