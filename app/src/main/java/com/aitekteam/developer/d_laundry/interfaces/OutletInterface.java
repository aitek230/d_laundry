package com.aitekteam.developer.d_laundry.interfaces;

import com.aitekteam.developer.d_laundry.models.M_Provider;

public interface OutletInterface {
    void onClick(M_Provider item, int position);
}
