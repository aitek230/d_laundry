package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private EditText
            login_email,
            login_password;
    private Button
            login_button;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.login_email                    = findViewById(R.id.login_email);
        this.login_password                 = findViewById(R.id.login_password);
        this.login_button                   = findViewById(R.id.login_button);
        Button login_registration_button    = findViewById(R.id.login_registration_button);
        this.mAuth = FirebaseAuth.getInstance();

        this.login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });

        login_registration_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void doLogin() {
        if (this.mAuth != null) {
            String email = this.login_email.getText().toString().trim();
            String password = this.login_password.getText().toString().trim();
            if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
                Toast.makeText(getApplicationContext(),
                        R.string.msg_failed_login, Toast.LENGTH_LONG).show();
                return;
            }
            this.login_button.setEnabled(false);
            this.login_button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryAlpha));
            mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            if (mAuth.getCurrentUser() != null) {
                                Toast.makeText(getApplicationContext(),
                                        R.string.msg_success_login, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(),
                                    R.string.msg_failed_login, Toast.LENGTH_LONG).show();
                            login_button.setEnabled(true);
                            login_button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        }
                    }
                });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
