package com.aitekteam.developer.d_laundry.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aitekteam.developer.d_laundry.BasketActivity;
import com.aitekteam.developer.d_laundry.DetailOrderActivity;
import com.aitekteam.developer.d_laundry.EditProfileActivity;
import com.aitekteam.developer.d_laundry.LoginActivity;
import com.aitekteam.developer.d_laundry.MainActivity;
import com.aitekteam.developer.d_laundry.R;
import com.aitekteam.developer.d_laundry.adapters.PackageUnitAdapters;
import com.aitekteam.developer.d_laundry.interfaces.PackageUnitInterface;
import com.aitekteam.developer.d_laundry.models.M_Package_Unit;
import com.aitekteam.developer.d_laundry.models.M_Provider;
import com.aitekteam.developer.d_laundry.models.M_Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;

public class NonMemberFragment extends Fragment {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ImageView user_face;
    private TextView
            user_name,
            user_email,
            user_phone,
            services_view_all;
    private GridView services_list;
    private ListView non_member_menus;
    private FirebaseUser currentUser;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = this.mAuth.getCurrentUser();
        if (currentUser == null) {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
            if (getActivity() != null)
                getActivity().finish();
        }
        this.mDatabase = FirebaseDatabase.getInstance().getReference();

        return inflater.inflate(R.layout.fragment_non_member, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.user_face = view.findViewById(R.id.user_face);
        this.user_email = view.findViewById(R.id.user_email);
        this.user_name = view.findViewById(R.id.user_name);
        this.user_phone = view.findViewById(R.id.user_phone);
        this.services_list = view.findViewById(R.id.services_list);
        this.non_member_menus = view.findViewById(R.id.non_member_menus);

        getServiceUnit();
        getNonMemberMenu();
        getProfile();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (currentUser != null) {
            getProfile();
        }
    }

    private void getProfile() {
        if (currentUser != null)
        mDatabase.child("m-users").child(this.currentUser.getUid())
                .addListenerForSingleValueEvent(profile);
    }

    private void getNonMemberMenu() {
        if (getContext() != null) {
            ArrayAdapter<String> itemsAdapter =
                    new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,
                            getResources().getStringArray(R.array.user_non_member_profile_array));
            non_member_menus.setAdapter(itemsAdapter);
            non_member_menus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == 2 || i == 1) {
                        Intent intent = new Intent(getContext(), DetailOrderActivity.class);
                        startActivity(intent);
                    }
                    else if (i == 3) {
                        mAuth.signOut();
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }
            });
        }
    }

    private int randomProvider() {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(2);
    }

    private void getServiceUnit() {
        mDatabase.child("m-provider").child(String.valueOf(randomProvider()))
                .addListenerForSingleValueEvent(provider);
    }

    private ValueEventListener provider = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 1) {
                final M_Provider selectedProvider = dataSnapshot.getValue(M_Provider.class);
                ArrayList<M_Package_Unit> items = new ArrayList<>();
                for (DataSnapshot item: dataSnapshot.child("package-unit").getChildren()) {
                    items.add(item.getValue(M_Package_Unit.class));
                }
                if (getContext() != null)
                    services_list.setAdapter(new PackageUnitAdapters(items, getContext(),
                        new PackageUnitInterface() {
                            @Override
                            public void onClick(M_Package_Unit item, int position) {
                                if (selectedProvider != null) {
                                    Intent intent = new Intent(getContext(), BasketActivity.class);
                                    intent.putExtra("selected_provider", selectedProvider.getProvider_id());
                                    startActivity(intent);
                                }
                            }
                        }));
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private ValueEventListener profile = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 1) {
                M_Users users = dataSnapshot.getValue(M_Users.class);
                if (users != null) {
                    user_email.setText(users.getUser_email());
                    user_name.setText(users.getUser_full_name());
                    user_phone.setText(users.getUser_phone());
                    user_face.setImageResource(R.drawable.user);

                    user_face.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getContext(), EditProfileActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
