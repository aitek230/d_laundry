package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aitekteam.developer.d_laundry.models.M_Package_Member;
import com.aitekteam.developer.d_laundry.models.M_Users;
import com.aitekteam.developer.d_laundry.models.M_Users_Transaction;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

public class RegistrationActivity extends AppCompatActivity {

    private EditText
            registration_name,
            registration_phone,
            registration_email,
            registration_password;
    private TextView
            registration_package;
    private DatabaseReference
            mDatabase;
    private int
            package_member_selected,
            userType;
    private FirebaseAuth
            mAuth;
    private Button registration_button;
    private ArrayList<M_Package_Member> m_package_members;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.mAuth = FirebaseAuth.getInstance();

        this.registration_name = findViewById(R.id.registration_name);
        this.registration_phone = findViewById(R.id.registration_phone);
        this.registration_email = findViewById(R.id.registration_email);
        this.registration_password = findViewById(R.id.registration_password);
        this.registration_package = findViewById(R.id.registration_package);
        Spinner registration_user_type = findViewById(R.id.registration_user_type);
        registration_button = findViewById(R.id.registration_button);
        this.package_member_selected = -1;

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.user_type_array, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        registration_user_type.setAdapter(adapter);

        registration_user_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) getPacketMember();
                userType = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        registration_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doRegistration();
            }
        });
    }

    private void doRegistration() {
        if (this.mAuth != null) {
            final String name = this.registration_name.getText().toString().trim();
            final String phone = this.registration_phone.getText().toString().trim();
            final String email = this.registration_email.getText().toString().trim();
            final String password = this.registration_password.getText().toString().trim();
            final String packageName = this.registration_package.getText().toString().trim();
            this.registration_button.setEnabled(false);
            this.registration_button.setBackgroundColor(getResources().getColor(R.color.colorPrimaryAlpha));
            mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String key = mAuth.getUid();
                            if (key != null) {
                                final M_Users newUser = new M_Users(
                                        key,
                                        name,
                                        email,
                                        password,
                                        phone,
                                        packageName,
                                        (userType == 0) ? 2 : userType,
                                        package_member_selected,
                                        (package_member_selected == -1) ? 0 : 1,
                                        0
                                );
//                                String transactionKey = mDatabase.child("m-users-transaction").child(key).push().getKey();
//                                M_Users_Transaction newUserTransaction = new M_Users_Transaction(
//                                        transactionKey,
//                                        packageName,
//                                        1,
//                                        System.currentTimeMillis()
//                                );
//                                mDatabase.child("m-users-transaction").child(key).child(transactionKey).setValue(newUserTransaction);
                                mDatabase.child("m-users").child(key).setValue(newUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Intent intent = new Intent(RegistrationActivity.this, PaymentQuotaActivity.class);
                                            Bundle args = new Bundle();
                                            args.putSerializable("QUOTA", (Serializable) m_package_members.get(package_member_selected));
                                            args.putSerializable("USERS", (Serializable) newUser);
                                            args.putSerializable("SOURCE", (Serializable) 0);
                                            intent.putExtra("BUNDLE", args);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                });
                            }
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    R.string.msg_failed_registration, Toast.LENGTH_LONG).show();
                            registration_button.setEnabled(true);
                            registration_button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        }
                    }
                });
        }
    }

    private void getPacketMember() {
        mDatabase.child("m-package-member").addValueEventListener(packageMember);
    }

    private void setListPackageMember(ArrayList<M_Package_Member> items) {
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_singlechoice);
        for (M_Package_Member item: items) {
            arrayAdapter.add(item.getPackage_name() +
                    " Rp. " +
                    item.getPackage_price() +
                    ".000");
        }
        if (arrayAdapter.getCount() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
            builder.setTitle(R.string.hint_choose_package)
                    .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            registration_package.setVisibility(View.VISIBLE);
                            registration_package.setText(arrayAdapter.getItem(i));
                            package_member_selected = i;
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private ValueEventListener packageMember = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 0) {
                ArrayList<M_Package_Member> items = new ArrayList<>();
                for (DataSnapshot item : dataSnapshot.getChildren()) {
                    items.add(item.getValue(M_Package_Member.class));
                }
                if (items.size() > 0) {
                    m_package_members = items;
                    setListPackageMember(items);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
