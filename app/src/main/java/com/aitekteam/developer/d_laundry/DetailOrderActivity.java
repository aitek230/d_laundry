package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.aitekteam.developer.d_laundry.adapters.UserPaymentPackageUnitAdapters;
import com.aitekteam.developer.d_laundry.interfaces.UsersPaymentPackageUnitInterface;
import com.aitekteam.developer.d_laundry.models.M_Package_Unit;
import com.aitekteam.developer.d_laundry.models.M_Users_Payment_Package_Unit;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DetailOrderActivity extends AppCompatActivity {

    private ListView order_list;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);

        this.order_list = findViewById(R.id.order_list);

        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = this.mAuth.getCurrentUser();
        this.mDatabase = FirebaseDatabase.getInstance().getReference();

        getOrderList();
    }

    private void getOrderList() {
        mDatabase.child("m-user-payment-package-unit")
                .child(mAuth.getUid())
                .addValueEventListener(order);
    }

    private ValueEventListener order = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 0) {
                ArrayList<M_Users_Payment_Package_Unit> items = new ArrayList<>();
                for (DataSnapshot item: dataSnapshot.getChildren()) {
                    items.add(item.getValue(M_Users_Payment_Package_Unit.class));
                }
                if (items.size() > 0) {
                    setOrderList(items);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void setOrderList(ArrayList<M_Users_Payment_Package_Unit> items) {
        this.order_list.setAdapter(new UserPaymentPackageUnitAdapters(items, this,
            new UsersPaymentPackageUnitInterface() {
                @Override
                public void onClick(M_Users_Payment_Package_Unit item, int position) {
                    int total = 0;
                    ArrayAdapter<String> package_units = new ArrayAdapter<String>(DetailOrderActivity.this,
                            android.R.layout.select_dialog_singlechoice);
                    int i = 0;
                    for (M_Package_Unit unit: item.getPackage_units()) {
                        total += unit.getItem_price();
                        package_units.add(unit.getItem_name() + " " + unit.getItem_price());
                        i = i+1;
                    }
                    package_units.add("Total " + total);
                    if (package_units.getCount() > 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DetailOrderActivity.this, R.style.MyDialogTheme);

                        builder.setTitle("Detail Orderan");
                        builder.setAdapter(package_units, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            }));
    }
}
