package com.aitekteam.developer.d_laundry.models;

import java.io.Serializable;

public class M_Provider implements Serializable {
    private String
            provider_address,
            provider_name;
    private int
            provider_id;

    public M_Provider(String provider_address, String provider_name, int provider_id) {
        this.provider_address = provider_address;
        this.provider_name = provider_name;
        this.provider_id = provider_id;
    }

    public M_Provider() {
    }

    public String getProvider_address() {
        return provider_address;
    }

    public void setProvider_address(String provider_address) {
        this.provider_address = provider_address;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public int getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(int provider_id) {
        this.provider_id = provider_id;
    }
}
