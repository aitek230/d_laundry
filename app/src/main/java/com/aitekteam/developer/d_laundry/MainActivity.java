package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;

import com.aitekteam.developer.d_laundry.adapters.MainPagerAdapter;
import com.aitekteam.developer.d_laundry.fragments.HelpFragment;
import com.aitekteam.developer.d_laundry.fragments.HomeFragment;
import com.aitekteam.developer.d_laundry.fragments.MemberFragment;
import com.aitekteam.developer.d_laundry.fragments.MessageFragment;
import com.aitekteam.developer.d_laundry.fragments.NonMemberFragment;
import com.aitekteam.developer.d_laundry.models.M_Users;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference
            mDatabase;
    private FirebaseAuth mAuth;
    private ViewPager main_pager;
    private BottomNavigationView main_menu;
    private MenuItem prevMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.mAuth = FirebaseAuth.getInstance();

        this.main_pager = findViewById(R.id.main_pager);
        this.main_menu = findViewById(R.id.main_menu);

        FirebaseUser currentUser = this.mAuth.getCurrentUser();
        if (currentUser != null) {
            this.mDatabase = FirebaseDatabase.getInstance().getReference();
            this.mDatabase.child("m-users").child(currentUser.getUid())
                    .addListenerForSingleValueEvent(profile);
        }
        else {
            ArrayList<Fragment> items = new ArrayList<>();
            items.add(new HomeFragment());
            items.add(new HelpFragment());
            items.add(new MessageFragment());
            items.add(new NonMemberFragment());

            MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), items);
            main_pager.setAdapter(adapter);
            main_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (prevMenuItem != null) {
                        prevMenuItem.setChecked(false);
                    }
                    else
                    {
                        main_menu.getMenu().getItem(0).setChecked(false);
                    }
                    main_menu.getMenu().getItem(position).setChecked(true);
                    prevMenuItem = main_menu.getMenu().getItem(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            main_menu.setOnNavigationItemSelectedListener(new BottomNavigationView
                    .OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.item_home:
                            main_pager.setCurrentItem(0);
                            break;
                        case R.id.item_help:
                            main_pager.setCurrentItem(1);
                            break;
                        case R.id.item_message:
                            main_pager.setCurrentItem(2);
                            break;
                        default:
                            main_pager.setCurrentItem(3);
                            break;
                    }
                    return false;
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null)
            this.mDatabase.child("m-users").child(currentUser.getUid()).removeEventListener(profile);
    }

    private ValueEventListener profile = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            ArrayList<Fragment> items = new ArrayList<>();
            items.add(new HomeFragment());
            items.add(new HelpFragment());
            items.add(new MessageFragment());
            if (dataSnapshot.getChildrenCount() > 1) {
                M_Users user = dataSnapshot.getValue(M_Users.class);
                if (user != null) {
                    if (user.getUser_type() == 1)
                        items.add(new MemberFragment());
                    else
                        items.add(new NonMemberFragment());
                }
            } else {
                items.add(new NonMemberFragment());
            }

            MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), items);
            main_pager.setAdapter(adapter);
            main_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (prevMenuItem != null) {
                        prevMenuItem.setChecked(false);
                    }
                    else
                    {
                        main_menu.getMenu().getItem(0).setChecked(false);
                    }
                    main_menu.getMenu().getItem(position).setChecked(true);
                    prevMenuItem = main_menu.getMenu().getItem(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            main_menu.setOnNavigationItemSelectedListener(new BottomNavigationView
                    .OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.item_home:
                            main_pager.setCurrentItem(0);
                            break;
                        case R.id.item_help:
                            main_pager.setCurrentItem(1);
                            break;
                        case R.id.item_message:
                            main_pager.setCurrentItem(2);
                            break;
                        default:
                            main_pager.setCurrentItem(3);
                            break;
                    }
                    return false;
                }
            });
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
