package com.aitekteam.developer.d_laundry.models;

import java.io.Serializable;
import java.util.ArrayList;

public class M_Users_Payment_Package_Unit implements Serializable {
    ArrayList<M_Package_Unit> package_units;
    M_Provider provider;
    M_Payment payment;
    String user_payment_id;
    Long user_payment_date;

    public M_Users_Payment_Package_Unit(ArrayList<M_Package_Unit> package_units, M_Provider provider, M_Payment payment, String user_payment_id, Long user_payment_date) {
        this.package_units = package_units;
        this.provider = provider;
        this.payment = payment;
        this.user_payment_id = user_payment_id;
        this.user_payment_date = user_payment_date;
    }

    public M_Users_Payment_Package_Unit() {
    }

    public ArrayList<M_Package_Unit> getPackage_units() {
        return package_units;
    }

    public void setPackage_units(ArrayList<M_Package_Unit> package_units) {
        this.package_units = package_units;
    }

    public M_Provider getProvider() {
        return provider;
    }

    public void setProvider(M_Provider provider) {
        this.provider = provider;
    }

    public M_Payment getPayment() {
        return payment;
    }

    public void setPayment(M_Payment payment) {
        this.payment = payment;
    }

    public String getUser_payment_id() {
        return user_payment_id;
    }

    public void setUser_payment_id(String user_payment_id) {
        this.user_payment_id = user_payment_id;
    }

    public Long getUser_payment_date() {
        return user_payment_date;
    }

    public void setUser_payment_date(Long user_payment_date) {
        this.user_payment_date = user_payment_date;
    }
}
