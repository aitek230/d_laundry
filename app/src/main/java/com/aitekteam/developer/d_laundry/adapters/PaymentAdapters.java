package com.aitekteam.developer.d_laundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aitekteam.developer.d_laundry.R;
import com.aitekteam.developer.d_laundry.interfaces.PaymentInterface;
import com.aitekteam.developer.d_laundry.models.M_Payment;

import java.util.ArrayList;

public class PaymentAdapters extends BaseAdapter {

    private ArrayList<M_Payment> items;
    private Context context;
    private LayoutInflater inflater;
    private PaymentInterface handler;

    public PaymentAdapters(ArrayList<M_Payment> items, Context context, PaymentInterface handler) {
        this.items = items;
        this.context = context;
        this.handler = handler;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int i) {
        return this.items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class Holder {
        TextView
                payment_logo,
                payment_name,
                payment_price;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View root = this.inflater.inflate(R.layout.item_outlet, null);
        holder.payment_logo = root.findViewById(R.id.provider_logo);
        holder.payment_name = root.findViewById(R.id.provider_name);
        holder.payment_price = root.findViewById(R.id.provider_address);

        holder.payment_logo.setText(this.items.get(i).getBank_name());
        holder.payment_name.setText(this.items.get(i).getBank_account_name());
        holder.payment_price.setText(this.items.get(i).getBank_number());

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.onClick(items.get(i), i);
            }
        });

        return root;
    }
}
