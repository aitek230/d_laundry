package com.aitekteam.developer.d_laundry.models;

import java.io.Serializable;

public class M_Package_Unit implements Serializable {
    private int item_id, item_price;
    private String item_name;

    public M_Package_Unit(int item_id, int item_price, String item_name) {
        this.item_id = item_id;
        this.item_price = item_price;
        this.item_name = item_name;
    }

    public M_Package_Unit() {
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getItem_price() {
        return item_price;
    }

    public void setItem_price(int item_price) {
        this.item_price = item_price;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }
}
