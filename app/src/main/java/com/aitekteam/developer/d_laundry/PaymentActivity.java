package com.aitekteam.developer.d_laundry;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.aitekteam.developer.d_laundry.adapters.OrderPackageUnitAdapters;
import com.aitekteam.developer.d_laundry.adapters.PaymentAdapters;
import com.aitekteam.developer.d_laundry.interfaces.PackageUnitInterface;
import com.aitekteam.developer.d_laundry.interfaces.PaymentInterface;
import com.aitekteam.developer.d_laundry.models.M_Package_Unit;
import com.aitekteam.developer.d_laundry.models.M_Payment;
import com.aitekteam.developer.d_laundry.models.M_Provider;
import com.aitekteam.developer.d_laundry.models.M_Users_Payment_Package_Unit;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class PaymentActivity extends AppCompatActivity {

    private TextView payment_total;
    private GridView payment_list;
    private ArrayList<M_Package_Unit> data;
    private M_Provider provider;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = this.mAuth.getCurrentUser();
        this.mDatabase = FirebaseDatabase.getInstance().getReference();

        this.payment_total = findViewById(R.id.payment_total);
        this.payment_list = findViewById(R.id.payment_list);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        if (args != null) {
            this.data = (ArrayList<M_Package_Unit>) args.getSerializable("ARRAYLIST");
            this.provider = (M_Provider) args.getSerializable("PROVIDER");
            payment_total.setText("Total : Rp." + data.get(data.size() - 1).getItem_price() + ".000");
        } else {
            this.data = new ArrayList<>();
        }

        getPayment();
    }

    private void getPayment() {
        this.mDatabase.child("m-payment").addValueEventListener(payment);
    }

    private ValueEventListener payment = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 0) {
                ArrayList<M_Payment> items = new ArrayList<>();
                for (DataSnapshot item: dataSnapshot.getChildren()) {
                    items.add(item.getValue(M_Payment.class));
                }

                payment_list.setAdapter(new PaymentAdapters(items, PaymentActivity.this,
                    new PaymentInterface() {
                        @Override
                        public void onClick(M_Payment item, int position) {
                            setPayment(item);
                        }
                    }));
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void setPayment(M_Payment item) {
        if (this.data.size() > 0 && mAuth != null) {
            this.data.remove(this.data.size() - 1);
            String key = mDatabase.child("m-user-payment-package-unit").child(mAuth.getUid()).push().getKey();
            M_Users_Payment_Package_Unit user_payment = new M_Users_Payment_Package_Unit(
                    this.data,
                    this.provider,
                    item,
                    key,
                    System.currentTimeMillis()
            );
            mDatabase.child("m-user-payment-package-unit").child(mAuth.getUid())
                    .child(key).setValue(user_payment).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Berhasil membuat tagihan", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Gagal membuat tagihan", Toast.LENGTH_LONG).show();
                    }
                    Intent intent = new Intent(PaymentActivity.this, DetailOrderActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }
}
