package com.aitekteam.developer.d_laundry.interfaces;

import com.aitekteam.developer.d_laundry.models.M_Payment;

public interface PaymentInterface {
    void onClick(M_Payment item, int position);
}
