package com.aitekteam.developer.d_laundry.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aitekteam.developer.d_laundry.BasketActivity;
import com.aitekteam.developer.d_laundry.LoginActivity;
import com.aitekteam.developer.d_laundry.R;
import com.aitekteam.developer.d_laundry.adapters.OutletAdapters;
import com.aitekteam.developer.d_laundry.adapters.ServiceAdapters;
import com.aitekteam.developer.d_laundry.interfaces.OutletInterface;
import com.aitekteam.developer.d_laundry.models.M_Provider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private GridView
            outlet_list,
            services_list;
    private EditText
            outlet_search;
    private DatabaseReference
            mDatabase;
    private ImageView outlet_search_button;
    private FirebaseAuth mAuth;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.mAuth = FirebaseAuth.getInstance();

        this.outlet_list = view.findViewById(R.id.outlet_list);
        this.services_list = view.findViewById(R.id.services_list);
        this.outlet_search = view.findViewById(R.id.outlet_search);
        TextView outlet_view_all = view.findViewById(R.id.outlet_view_all);
        outlet_search_button = view.findViewById(R.id.outlet_search_button);
        TextView services_view_all = view.findViewById(R.id.services_view_all);

        getProviders();
        getServices();

        outlet_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        services_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    private void getServices() {
        this.mDatabase.child("m-services").addValueEventListener(services);
    }

    private void setServices(ArrayList<String> items) {
        if (getContext() != null)
            this.services_list.setAdapter(new ServiceAdapters(items, getContext()));
    }

    private void getProviders() {
        this.mDatabase.child("m-provider").addValueEventListener(providers);
    }

    private void setProviders(final ArrayList<M_Provider> items) {
        if (getContext() != null) {
            final OutletAdapters outletAdapters = new OutletAdapters(items, getContext(),
                new OutletInterface() {
                    @Override
                    public void onClick(M_Provider item, int position) {
                        if (mAuth.getCurrentUser() != null) {
                            Intent intent = new Intent(getContext(), BasketActivity.class);
                            intent.putExtra("selected_provider", item.getProvider_id());
                            startActivity(intent);
                            getActivity().finish();
                        }
                        else {
                            Intent intent = new Intent(getContext(), LoginActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                });
            this.outlet_list.setAdapter(outletAdapters);

            outlet_search_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!TextUtils.isEmpty(outlet_search.getText().toString().trim())) {
                        String key = outlet_search.getText().toString().trim();
                        ArrayList<M_Provider> founds = new ArrayList<>();
                        for (M_Provider item: items) {
                            if (item.getProvider_name().contains(key))
                                founds.add(item);
                            else if (item.getProvider_address().contains(key))
                                founds.add(item);
                        }
                        outletAdapters.setItems(founds);
                    }
                    else {
                        outletAdapters.setItems(items);
                    }
                }
            });
        }
    }

    private ValueEventListener services = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 0) {
                ArrayList<String> items = new ArrayList<>();
                for (DataSnapshot item: dataSnapshot.getChildren()) {
                    items.add(item.child("service_name").getValue(String.class));
                }
                if (items.size() > 0) {
                    setServices(items);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private ValueEventListener providers = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getChildrenCount() > 0) {
                ArrayList<M_Provider> items = new ArrayList<>();
                for (DataSnapshot item: dataSnapshot.getChildren()) {
                    items.add(item.getValue(M_Provider.class));
                }
                if (items.size() > 0) {
                    setProviders(items);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
