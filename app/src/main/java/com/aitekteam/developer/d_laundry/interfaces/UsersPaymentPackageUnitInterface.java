package com.aitekteam.developer.d_laundry.interfaces;

import com.aitekteam.developer.d_laundry.models.M_Users_Payment_Package_Unit;

public interface UsersPaymentPackageUnitInterface {
    void onClick(M_Users_Payment_Package_Unit item, int position);
}
