package com.aitekteam.developer.d_laundry.models;

import java.io.Serializable;

public class M_Users_Transaction implements Serializable {
    private String transaction_id,
            transaction_description;
    private int transaction_type;
    private Long transaction_date;

    public M_Users_Transaction(String transaction_id, String transaction_description, int transaction_type, Long transaction_date) {
        this.transaction_id = transaction_id;
        this.transaction_description = transaction_description;
        this.transaction_type = transaction_type;
        this.transaction_date = transaction_date;
    }

    public M_Users_Transaction() {
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTransaction_description() {
        return transaction_description;
    }

    public void setTransaction_description(String transaction_description) {
        this.transaction_description = transaction_description;
    }

    public int getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(int transaction_type) {
        this.transaction_type = transaction_type;
    }

    public Long getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(Long transaction_date) {
        this.transaction_date = transaction_date;
    }
}
