package com.aitekteam.developer.d_laundry.interfaces;

import com.aitekteam.developer.d_laundry.models.M_Package_Unit;

public interface PackageUnitInterface {
    void onClick(M_Package_Unit item, int position);
}
