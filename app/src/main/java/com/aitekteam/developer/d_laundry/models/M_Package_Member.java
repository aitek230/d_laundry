package com.aitekteam.developer.d_laundry.models;

import java.io.Serializable;

public class M_Package_Member implements Serializable {
    private int
                package_id,
                package_kuota,
                package_price;
    private String
                package_name,
                package_unit;

    public M_Package_Member(int package_id, int package_kuota, int package_price, String package_name, String package_unit) {
        this.package_id = package_id;
        this.package_kuota = package_kuota;
        this.package_price = package_price;
        this.package_name = package_name;
        this.package_unit = package_unit;
    }

    public M_Package_Member() {
    }

    public int getPackage_id() {
        return package_id;
    }

    public void setPackage_id(int package_id) {
        this.package_id = package_id;
    }

    public int getPackage_kuota() {
        return package_kuota;
    }

    public void setPackage_kuota(int package_kuota) {
        this.package_kuota = package_kuota;
    }

    public int getPackage_price() {
        return package_price;
    }

    public void setPackage_price(int package_price) {
        this.package_price = package_price;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getPackage_unit() {
        return package_unit;
    }

    public void setPackage_unit(String package_unit) {
        this.package_unit = package_unit;
    }
}
