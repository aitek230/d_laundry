package com.aitekteam.developer.d_laundry.interfaces;

import com.aitekteam.developer.d_laundry.models.M_Users_Transaction;

public interface UserTransactionInterface {
    void onClick(M_Users_Transaction item, int position);
}
